import { Controller, Get, Post, Body, Res  } from '@nestjs/common';
import { AppService } from './app.service';
import { ProductosService } from './productos/productos.service';
import { join } from 'path';
import { Response } from 'express';
import * as fs from 'fs';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly productosService: ProductosService,
  ) {}

  @Get()
  getHello(@Res() res: Response) {
    const message = this.appService.getHello();
    const productos = this.productosService.findAll();

    const filePath = join(__dirname, '..', 'src', 'productos', 'views', 'index.html');
    fs.readFile(filePath, 'utf-8', (err, data) => {
      if (err) {
        console.error(err);
        res.status(500).send('Error loading the page');
        return;
      }

      const productosHtml = productos.map(producto => `
        <tr>
          <th scope="row">${producto.id}</th>
          <td>${producto.nombre}</td>
          <td>${producto.categoria}</td>
          <td>${producto.precio}</td>
          <td><img src="/static/imagen/editar.png" alt="Modificar" width="20" onclick="editarProducto(${producto.id}, '${producto.nombre}', '${producto.categoria}', '${producto.precio}')"></td>

          <td> <img src="/static/imagen/delete.png" alt="Delete" width="20" onclick="eliminarProducto(${producto.id})"></td>

        </tr>
      `).join('');

      const html = data
        .replace('{{message}}', message)
        .replace('{{productos}}', productosHtml);

      res.send(html);
    });
  }

  @Post('/create')
  createProducto(@Body() body, @Res() res: Response) {
    const { id, nombre, categoria, precio } = body;
    this.productosService.create({ id, nombre, categoria, precio });
    res.redirect('/');
  }

  
}