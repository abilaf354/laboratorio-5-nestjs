import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductosModule } from './productos/productos.module';
//importamos
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';


@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'node_modules'),
      serveRoot: '/node_modules',
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'src', 'productos', 'views'),
      serveRoot: '/static',
    }),
    ProductosModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
