import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common';
@Injectable()
export class ProductosService {
    private productos = [
        { id: 1, nombre: 'Galletas de Chocolate', categoria: 'Galletas crujientes con trozos de chocolate semi-amargo.', precio:12 },
       
      
      ];
    
      findAll() {
        return this.productos;
      }
    
      findById(id: number) {
        const producto = this.productos.find(p => p.id === id);
        if (!producto) {
          throw new NotFoundException(`Producto con el id '${id}' no encontrado`);
        }
        return producto;
      }
    
      create(producto) {
        this.productos.push({ ...producto, id: this.productos.length + 1 });
      }
    
      update(id: number, updateProducto) {
        const index = this.productos.findIndex(p => p.id === id);
        if (index === -1) {
          throw new NotFoundException(`Producto con el id '${id}' no encontrado`);
        }
        this.productos[index] = { ...this.productos[index], ...updateProducto };
      }
    
      delete(id: number) {
        const index = this.productos.findIndex(p => p.id === id);
        if (index === -1) {
          throw new NotFoundException(`Producto con el id '${id}' no encontrado`);
        }
        this.productos.splice(index, 1);
      } 
    }

