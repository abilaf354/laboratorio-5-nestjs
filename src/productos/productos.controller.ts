import { Controller } from '@nestjs/common';
import {  Get, Post, Param, Body, Put, Delete } from '@nestjs/common';
import { ProductosService } from './productos.service';
@Controller('productos')
export class ProductosController {
    constructor(private readonly productosService: ProductosService) {}

  @Get()
  getAllProductos() {
    return this.productosService.findAll();
  }

  @Get(':id')
  getProductoById(@Param('id') id: string) {
    return this.productosService.findById(+id);
  }

  @Post()
  createProducto(@Body() producto) {
    return this.productosService.create(producto);
  }

  @Put(':id')
  updateProducto(@Param('id') id: string, @Body() updateProducto) {
    return this.productosService.update(+id, updateProducto);
  }

  @Delete(':id')
  deleteProducto(@Param('id') id: string) {
  return this.productosService.delete(+id);
}
}
